# BASIC WebRTC CHAT AND MULTIPLAYER GAME BUILT WITH PeerJS  #

A basic WebRTC chat application and a multiplayer game of tic-tac-toe implemented using Data Channels that facilitates real-time gaming and communication.