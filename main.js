var peer = new Peer({
  key: 'gow94i7estznnrk9',
  debug: 3,
  logFunction: function(){
    var copy = Array.prototype.slice.call(arguments).join('');
    var para = document.createElement("p");
    var node = document.createTextNode(copy);
    para.appendChild(node);
    var element = document.getElementById("log");
    element.appendChild(para);
  }
});

var connectedPeers = {};

peer.on('open', function(id){
  document.getElementById("pid").innerHTML = id;
});

peer.on('connection', connect);

peer.on('error', function(err){
  console.log(err);
});


function connect(c){
  if(c.label === 'chat'){
    var chatbox = document.getElementById("connection");
    document.getElementById("friend_name").innerHTML = c.peer;
    var msg_para = document.createElement("p");
    var messages_node = document.createTextNode("Peer Connected");
    msg_para.appendChild(messages_node);
    chatbox.appendChild(msg_para);

    document.getElementById("filler").style.display = "none";

    c.on('data', function(data){
      var rec_msg_para = document.createElement("p");
      var rec_msg_node = document.createTextNode(c.peer + ": " + data);
      rec_msg_para.appendChild(rec_msg_node);
      chatbox.appendChild(rec_msg_para);
    });

    c.on('close', function(){
      alert(c.peer+' has left the chat.');
      chatbox.innerHTML='';
      document.getElementById("filler").style.display = "block";
      delete connectedPeers[c.peer];
      location.reload();
    });
  }

  if(c.label === 'game'){
    document.getElementById('gameSec').removeAttribute('class');
    c.on('data', function(data){
      console.log(data);
      handleReceivedData(data);
    });
  }

  if(c.label === 'gameMsg'){
    document.getElementById('gameSec').removeAttribute('class');
    c.on('data', function(data){
      document.getElementById('notifs').innerHTML=data;
      resetGameBoard();
    });
  }

  connectedPeers[c.peer] = 1; 
};

function handleReceivedData(data){
  if(data=="stop_play"){
    document.getElementById('game').setAttribute('class', 'disab2');
    document.getElementById('main_box').style.background = "white";
    document.getElementById('notifs').innerHTML = "Click Start Play";
  }
  else if(data=="start_play"){
    document.getElementById('game').removeAttribute('class', 'disab2');
    document.getElementById('main_box').style.background = "rgba(0,100,100,0.4)";
    document.getElementById('notifs').innerHTML = "Game Started";
  }
  if(data[0]==0){
    document.getElementById(data[1]).innerHTML = player1;
    document.getElementById(data[1]).setAttribute("class", "disabled");
    document.getElementById('game').removeAttribute('class', 'disab2');
    document.getElementById('notifs').innerHTML = "Your Turn";
    turn = 1;
  }
  else if(data[0]==1){
    document.getElementById(data[1]).innerHTML = player2;
    document.getElementById(data[1]).setAttribute("class", "disabled");
    document.getElementById('game').removeAttribute('class', 'disab2');
    document.getElementById('notifs').innerHTML = "Your Turn";
    turn = 0;
  }
};

var clickConnectBtn = document.querySelector('#connect_users');
var otherPeerId = document.querySelector('#rid');
var closeConnBtn = document.querySelector('#close');
var msgBtn = document.querySelector('#sendMsgBtn');
var msgInput = document.querySelector('#text');

clickConnectBtn.addEventListener("click", function(event){
  var requestedPeer = otherPeerId.value;
  if(!connectedPeers[requestedPeer]){
    c = peer.connect(requestedPeer, {
      label: 'chat',
      serialization: 'none',
      metadata: {message:'h1'}
    });
    console.log(c);
    c.on('open', function(){
      connect(c);
    });
    c.on('error', function(err){
      alert(err);
    });

    var g = peer.connect(requestedPeer, {label:'game', reliable:false});
    g.on('open', function(){
      connect(g);
    });
    g.on('error', function(err){
      alert(err);
    });

    var g2 = peer.connect(requestedPeer, {label:'gameMsg', reliable:false});
    g2.on('open', function(){
      connect(g2);
    });
    g2.on('error', function(err){
      alert(err);
    });
  }
  connectedPeers[requestedPeer] = 1;
});


closeConnBtn.addEventListener("click", function(){
  var c2 = conn_params(0);
  c2.close();
  location.reload();
})


msgBtn.addEventListener("click", function(){
  var msg = msgInput.value;
  var c2 = conn_params(0);
  c2.send(msg);
  var my_msg_element = document.getElementById("connection");
  var my_msg_para = document.createElement("p");
  var my_msg_node = document.createTextNode("You: "+msg);
  my_msg_para.appendChild(my_msg_node);
  my_msg_element.appendChild(my_msg_para);
});

function conn_params(arrKey){
  var other_id = document.querySelector("#friend_name");
  var val = other_id.innerHTML;
  var conns = peer.connections[val];
  var conn = conns[arrKey];
  
  console.log(val);
  console.log(conns);
  console.log(peer.connections);
  console.log(conn);
  
  return conn;
};


/* GAME STARTS HERE*/

  var player1 = 'x';
  var player2 = 'o';
  var blockFiller = '+';
  var turn = 0; //toggle this between turns;
  var stopPlay = false;
  var count = 0;
  
  var gameBlocks = document.getElementsByTagName("LI");
  var startPlay = document.querySelector('#startBtn');


  startPlay.addEventListener("click", function(){
    toggleGameBoard(false);
  });


  function toggleGameBoard(val){  
    stopPlay = val;
    if(stopPlay==true){
      document.getElementById('game').setAttribute('class', 'disab2');
      document.getElementById('main_box').style.background = "white";
      document.getElementById('notifs').innerHTML = "Click Start Play";
      var c3 = conn_params(1);
      c3.send("stop_play");
    }
    else{
      document.getElementById('game').removeAttribute('class', 'disab2');
      document.getElementById('main_box').style.background = "rgba(0,100,100,0.4)";
      document.getElementById('notifs').innerHTML = "Game Started";
      var c3 = conn_params(1);
      c3.send("start_play");
    }
  }

  function resetGameBoard(){
    for(var i=0;i<=8;i++){
      gameBlocks[i].innerHTML = blockFiller;
      gameBlocks[i].removeAttribute('class', 'disabled');
      gameBlocks[i].setAttribute('class', 'game_piece');
    }
    document.getElementById('game').setAttribute('class', 'disab2');
    document.getElementById('main_box').style.background = "white";
    turn = 0;
    count = 0;
  }


  function gameClicks(boxId){
    changeBoxValue(boxId);
    if(turn == 0){
      turn = 1;
    }
    else{
      turn = 0;
    }
    var currVals = checkCurrentValues();
    var winner = checkWinDraw(currVals);

    if(winner == player1){
      var c4 = conn_params(2);
      document.getElementById('notifs').innerHTML = "You Win";
      c4.send("You Lost.Winner Player1");
      resetGameBoard();
      //toggleGameBoard(true);
    }
    if(winner == player2){
      var c4 = conn_params(2);
      document.getElementById('notifs').innerHTML = "You Win";
      c4.send("You Lost.Winner Player2");
      resetGameBoard();
      //toggleGameBoard(true);
    }
    if(count == 45 && winner==false){
      var c4 = conn_params(2);
      document.getElementById('notifs').innerHTML = "Tie";
      c4.send("Tie");
      resetGameBoard();
      //toggleGameBoard(true);
    }
  }

  function changeBoxValue(gameBoxId){
    var c3 = conn_params(1);
    if(turn == 0){
      document.getElementById(gameBoxId).innerHTML = player1;
      document.getElementById(gameBoxId).setAttribute("class", "disabled");
      document.getElementById('game').setAttribute('class', 'disab2');
      document.getElementById('notifs').innerHTML = "Player2's Turn";
      var data1 = [0,gameBoxId];
      c3.send(data1);
    }else{
      document.getElementById(gameBoxId).innerHTML = player2;
      document.getElementById(gameBoxId).setAttribute("class", "disabled");
      document.getElementById('game').setAttribute('class', 'disab2');
      document.getElementById('notifs').innerHTML = "Player1's Turn";
      var data2 = [1,gameBoxId];
      c3.send(data2);
    }
  }

  function checkCurrentValues(){
    var valArray = new Array(1);
    for(var i=0;i<=8;i++){
      if(gameBlocks[i].innerHTML!=blockFiller){
        count += 1;
        console.log(count);
      }
      valArray.push(gameBlocks[i].innerHTML);
    }
    return valArray;
  }

  function checkWinDraw(currVals){
    var winCombos = [
      [1,2,3],[4,5,6],[7,8,9],[1,4,7],[2,5,8],[3,6,9],[1,5,9],[3,5,7]
    ];
    var pos = new Array();
    for(var i=0;i<=7;i++){
      for(var j=0;j<=2;j++){
        pos.push(winCombos[i][j]);
        if((currVals[pos[0]]==currVals[pos[1]])&&(currVals[pos[0]]==currVals[pos[2]])){
          return currVals[pos[0]];
        }
        if(i==7 && j==2 && count==45 && (currVals[pos[0]]!=currVals[pos[1]] || currVals[pos[0]]!=currVals[pos[2]])){
          return false;
        }
      }
      pos = [];
    }
  }

/* GAME ENDS HERE*/